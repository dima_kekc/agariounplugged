/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GTGE;

import DishObjects.DishObject;
import agariounplugged.CollisionManager;
import com.golden.gamedev.object.Sprite;
import com.golden.gamedev.object.SpriteGroup;
import com.golden.gamedev.object.collision.BasicCollisionGroup;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;
import views.DishObjectView;

/**
 *
 * @author Dmitry
 */
public class GTGECollisionManager extends CollisionManager {

    private SpriteGroup spriteGroup = new SpriteGroup("Object");
    private GTGECollisionChecker gtgeCollisionChecker = new GTGECollisionChecker(spriteGroup);

    @Override
    public void checkCollision() {
        this.gtgeCollisionChecker.checkCollision();
    }

    @Override
    public void addDishObject(DishObject dishObject) {
        this.spriteGroup.add((Sprite) dishObject.getSpriteModel());
    }

    @Override
    public void removeDishObject(DishObject dishObject) {
        this.spriteGroup.remove((Sprite) dishObject.getSpriteModel());
    }
    
    private class GTGECollisionChecker extends BasicCollisionGroup {
        
        GTGECollisionChecker(SpriteGroup sg) {
            this.setCollisionGroup(sg, sg);
            this.pixelPerfectCollision = true;
        }

        @Override
        public void collided(Sprite first, Sprite second) {
            DishObjectView firstView = (DishObjectView) first;
            DishObjectView secondView = (DishObjectView) second;
            try {
                GTGECollisionManager.this.collided(firstView.getDishObject(), secondView.getDishObject());
            } catch (NoSuchMethodException ex) {
                Logger.getLogger(GTGECollisionManager.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(GTGECollisionManager.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(GTGECollisionManager.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                Logger.getLogger(GTGECollisionManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
