/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GTGE;

import DishObjects.AliveObject;
import DishObjects.Bacterium;
import GTGE.Views.GTGEDishView;
import Specializations.Specialization;
import agariounplugged.*;
import agariounplugged.Factories.AgarioFactory;
import com.golden.gamedev.Game;
import com.golden.gamedev.GameLoader;
import com.golden.gamedev.object.Background;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Point;
import static java.awt.event.KeyEvent.VK_SPACE;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Dmitry
 */
public class GTGEGameApplication extends GameApplication {

    private GTGEGame gtgeGame;

    public GTGEGameApplication(AgarioFactory agarioFactory) {
        super(agarioFactory);
        this.gtgeGame = new GTGEGame();
    }

    public GTGEGame getGTGEGame() {
        return gtgeGame;
    }

    private class GTGEGame extends Game implements DishObjectController {

        @Override
        public void initResources() {

        }

        @Override
        public void update(long l) {
            GTGEGameApplication.this.update(l);
        }

        @Override
        public void render(Graphics2D gd) {
            GTGEDishView dishView = (GTGEDishView) GTGEGameApplication.this.gameFrame.getDishView();
            Bacterium player = GTGEGameApplication.this.gameModel.getPlayer();
            int playerX = player.getPosition().x;
            int playerY = player.getPosition().y;
            dishView.getBackground().setToCenter(playerX, playerY, player.getSize(), player.getSize());
            GTGEGameApplication.this.render(gd);
        }

        @Override
        public void updateDishObjectPosition(Bacterium bacterium) {
            GTGEDishView dishView = (GTGEDishView) GTGEGameApplication.this.gameFrame.getDishView();
            Background bg = dishView.getBackground();
            // Определить положение фона относительно окна
            int bgx = (int) bg.getX();
            int bgy = (int) bg.getY();

            // Определить координаты мыши на поле
            int mx = bsInput.getMouseX();
            int my = bsInput.getMouseY();
            int x = mx + bgx;
            int y = my + bgy;
            bacterium.setDestination(new Point(x, y));
        }

        @Override
        public void initThrowBolid(Bacterium bacterium) {
            if (bsInput.getKeyPressed() == VK_SPACE) {
                GTGEDishView dishView = (GTGEDishView) GTGEGameApplication.this.gameFrame.getDishView();
                Background bg = dishView.getBackground();
                // Определить положение фона относительно окна
                int bgx = (int) bg.getX();
                int bgy = (int) bg.getY();

                // Определить координаты мыши на поле
                int mx = bsInput.getMouseX();
                int my = bsInput.getMouseY();
                int x = mx + bgx;
                int y = my + bgy;
                bacterium.throwBolid(new Point(x, y));
            }
        }

        @Override
        public void chooseSpecialization(ArrayList<Specialization> availableUpgrades, AliveObject obj) {
            int n;
            do {
                // Сфрпмировать список названий доступных специализаций
                Object[] options = new Object[availableUpgrades.size()];
                for (int i = 0; i < availableUpgrades.size(); ++i) {
                    options[i] = availableUpgrades.get(i).getName();
                }

                // Запросить у пользователя новую специализацию
                n = JOptionPane.showOptionDialog(null, "Выберите новую специализацию.", "Выбор специализации.",
                        JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

                // Если специализация выбрана - установить её
                if (n != JOptionPane.CLOSED_OPTION) {
                    obj.setSpecialization(availableUpgrades.get(n));
                }
            } while (n == JOptionPane.CLOSED_OPTION);
        }
    }

    @Override
    public void start() {
        super.start();
        GameLoader gtgeGameLoader = new GameLoader();
        gtgeGameLoader.setup(this.gtgeGame, new Dimension(GameApplication.screenWidth, GameApplication.screenHeight), false);
        gtgeGameLoader.start();

    }

}
