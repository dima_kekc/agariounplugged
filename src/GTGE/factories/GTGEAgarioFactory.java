/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GTGE.factories;

import GTGE.GTGECollisionManager;
import GTGE.GTGEGameApplication;
import GTGE.Views.GTGEDishView;
import GTGE.Views.GTGESprite;
import agariounplugged.AIController;
import agariounplugged.CollisionManager;
import agariounplugged.Dish;
import agariounplugged.DishObjectController;
import agariounplugged.Factories.AgarioFactory;
import agariounplugged.GameApplication;
import views.DishView;
import views.SpriteModel;

/**
 *
 * @author Dmitry
 */
public class GTGEAgarioFactory extends AgarioFactory {
   
    private GTGEGameApplication gtgeGameApplication;

    @Override
    public GameApplication createGameApplication() {
        GTGEGameApplication result = new GTGEGameApplication(this);
        this.gtgeGameApplication = result;
        return result;
    }

    @Override
    public DishView createDishView() {
        GTGEDishView result = new GTGEDishView();
        Dish.getInstance().addObjectAddedListener(result);
        Dish.getInstance().addObjectRemovedListener(result);
        return result;
    }

    @Override
    public SpriteModel createSpriteModel() {
        return (SpriteModel) new GTGESprite();
    }

    @Override
    public DishObjectController createPlayerController() {
        return (DishObjectController) this.gtgeGameApplication.getGTGEGame();
    }

    @Override
    public CollisionManager createCollisionManager() {
        return new GTGECollisionManager();
    }
    
}
