/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GTGE.factories;

import GTGE.Views.GTGESprite;
import DishObjects.DishObject;
import agariounplugged.Factories.DishObjectViewFactory;
import java.awt.Color;
import DishObjects.Agar;
import DishObjects.Bacterium;
import DishObjects.Bolid;
import DishObjects.CarbonDioxide;
import DishObjects.Light;
import DishObjects.Oxygen;
import DishObjects.Water;
import views.DishObjectView;

/**
 *
 * @author Dmitry
 */
public class GTGEDishObjectViewFactory extends DishObjectViewFactory {

    @Override
    public DishObjectView createDishObjectView(DishObject dishObject) {

        GTGESprite dishObjectView = (GTGESprite) dishObject.getSpriteModel();
        dishObjectView.setDishObject(dishObject);
        //Настроить внешний вид
        if (dishObject instanceof Agar) {
            dishObjectView.setColor(Color.LIGHT_GRAY); // Магию выпилить...
        } else if (dishObject instanceof CarbonDioxide) {
            dishObjectView.setColor(Color.DARK_GRAY); // Магию выпилить...
        }
        else if (dishObject instanceof Water) {
            dishObjectView.setColor(Color.BLUE); // Магию выпилить...
        }
        else if (dishObject instanceof Light) {
            dishObjectView.setColor(Color.YELLOW); // Магию выпилить...
        }
        else if (dishObject instanceof Oxygen) {
            dishObjectView.setColor(Color.WHITE);
        }
        else if (dishObject instanceof Bolid) {
            dishObjectView.setColor(Color.red);
        }
        else if (dishObject instanceof Bacterium) {
            dishObjectView.setColor(Color.green);
        } 
        dishObjectView.createSpriteView();
        return dishObjectView;
    }

}
