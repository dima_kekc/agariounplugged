/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GTGE.Views;

import DishObjects.AliveObject;
import DishObjects.DishObject;
import com.golden.gamedev.object.Sprite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import views.DishObjectView;
import views.SpriteModel;

/**
 *
 * @author Dmitry
 */
public class GTGESprite extends Sprite implements DishObjectView, SpriteModel {

    private Color color;
    private DishObject dishObject;

    public GTGESprite() {
        super();
    }

    public void createSpriteView() {

        // Зарисовать площадь нужным цветом
        BufferedImage bi = new BufferedImage(this.dishObject.getSize(), this.dishObject.getSize(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = bi.createGraphics();
        g2d.setColor(color);
        g2d.fillOval(0, 0, this.dishObject.getSize(), this.dishObject.getSize());

        // Обозначить периметр
        g2d.setColor(color.darker().darker());
        g2d.setStroke(new BasicStroke(3));
        g2d.drawOval(1, 1, this.dishObject.getSize() - 2, this.dishObject.getSize() - 2);
        // Установить аватар
        if (this.getDishObject() instanceof AliveObject) {
            try {
                BufferedImage avatar = ImageIO.read(new File("resources/avatars/" + dishObject.getType() + ".png"));
                g2d.drawImage(avatar, (this.dishObject.getSize() - avatar.getWidth()) / 2,
                        (this.dishObject.getSize() - avatar.getHeight()) / 2, null);
            } catch (IOException ex) {
                Logger.getLogger(GTGESprite.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        this.setImage(bi);
    }

    @Override
    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public void setIcon(BufferedImage icon) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setPosition(Point p) {
        this.setX(p.x);
        this.setY(p.y);
    }

    @Override
    public Point getPosition() {
        return new Point((int) this.getX(), (int) this.getY());
    }

    @Override
    public void setSpeed(double horizontalSpeed, double verticalSpeed) {
        this.setHorizontalSpeed(horizontalSpeed);
        this.setVerticalSpeed(verticalSpeed);
    }

    @Override
    public double getHorizontalSpeed() {
        return super.getHorizontalSpeed();
    }

    public double getVerticalSpeed() {
        return super.getVerticalSpeed();
    }

    @Override
    public void update(long l) {
        super.update(l);
        // Сделать ресайз спрайта, если в модели размер изменился
        if (this.dishObject.getSize() != this.width) {
            this.createSpriteView();
        }
    }

    @Override
    public void setDishObject(DishObject dishObject) {
        this.dishObject = dishObject;
    }

    @Override
    public DishObject getDishObject() {
        return this.dishObject;
    }

}
