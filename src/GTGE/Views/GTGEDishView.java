/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GTGE.Views;

import GTGE.factories.GTGEDishObjectViewFactory;
import DishObjects.DishObject;
import agariounplugged.Factories.DishObjectViewFactory;
import agariounplugged.GameApplication;
import com.golden.gamedev.object.Background;
import com.golden.gamedev.object.Sprite;
import com.golden.gamedev.object.background.ImageBackground;
import java.awt.Graphics2D;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import views.DishObjectView;
import views.DishView;

/**
 *
 * @author Dmitry
 */
public class GTGEDishView extends DishView {

    private String BACKGROUND_RESOURCE = "resources/background.jpg";

    private GTGEDishObjectViewFactory dishObjectViewFactory = new GTGEDishObjectViewFactory();
    private ArrayList<DishObjectView> dishObjectViews = new ArrayList<DishObjectView>();
    private Background background = null;

    public Background getBackground() {
        return this.background;
    }

    public GTGEDishView() {
        try {
            this.background = new ImageBackground(ImageIO.read(new File(BACKGROUND_RESOURCE)));
            this.background.setClip(0, 0, GameApplication.screenWidth, GameApplication.screenHeight);
        } catch (IOException ex) {
            Logger.getLogger(GTGEDishView.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public void render(Graphics2D gd) {
        background.render(gd);
        for (DishObjectView view : dishObjectViews) {
            view.render(gd);
        }
    }

    @Override
    public void dishObjectAdded(DishObject dishObject) {
        DishObjectView dishObjectView = dishObjectViewFactory.createDishObjectView(dishObject);
        Sprite sprite = (Sprite) dishObjectView;
        sprite.setBackground(background);
        dishObjectViews.add(dishObjectView);
    }

    @Override
    public void dishObjectRemoved(DishObject dishObject) {
        // Найти существующее view по объекту
        for (DishObjectView view : dishObjectViews) {
            if (dishObject.equals(view.getDishObject())) {
                dishObjectViews.remove(view);
                return;
            }
        }
    }

}
