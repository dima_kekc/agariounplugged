package events;

import DishObjects.AliveObject;
import Specializations.Specialization;
import java.util.ArrayList;

/**
 * Слушатель запроса специализации
 */
public interface AskForSpecializationListener {
    void askForSpecialization(AskForSpecializationEvent e, ArrayList <Specialization> availableUpgrades, AliveObject obj);
}
