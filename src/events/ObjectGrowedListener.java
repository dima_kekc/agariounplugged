package events;

import DishObjects.AliveObject;



/**
 * Слушатель роста бактерии
 */
public interface ObjectGrowedListener {
    void growed(ObjectGrowedEvent e, AliveObject aliveObject);
}
