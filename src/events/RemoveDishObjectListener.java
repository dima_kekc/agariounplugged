/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package events;

import DishObjects.DishObject;

/**
 *
 * @author Dmitry
 */
public interface RemoveDishObjectListener {
    public void dishObjectRemoved(DishObject dishObject);
}
