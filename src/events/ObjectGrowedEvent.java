package events;

import java.util.EventObject;

/**
 * Событие роста бактерии
 */
public class ObjectGrowedEvent extends EventObject {
    public ObjectGrowedEvent(Object source) { 
        super(source); 
    }
}
