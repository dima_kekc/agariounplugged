package events;

import java.util.EventObject;

/**
 * Запрос новой специализации
 */
public class AskForSpecializationEvent extends EventObject {
    public AskForSpecializationEvent(Object source) { 
        super(source); 
    }
}
