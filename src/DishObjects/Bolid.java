/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DishObjects;

import agariounplugged.Dish;
import agariounplugged.Factories.AgarioFactory;
import java.awt.Point;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dmitry
 */
public class Bolid extends Bacterium {

    public Bolid(AgarioFactory agarioFactory) {
        super(agarioFactory);
        parentCollision = true;
    }

    private Bacterium parent;
    private boolean parentCollision;
    private Point fromPoint;

    public boolean isParentCollision() {
        return this.parentCollision;
    }

    public void setParentCollision(boolean pc) {
        this.parentCollision = pc;
    }

    public void setParent(Bacterium parent) {
        this.parent = parent;
        this.fromPoint = new Point(parent.getPosition());
    }

    public Bacterium getParent() {
        return this.parent;
    }

    @Override
    public void collideWith(DishObject dishObject) {
        if (dishObject instanceof Bacterium && parent != dishObject && size < dishObject.getSize()) {
            ((Bacterium) dishObject).throwBolid(new Point(dishObject.destination));
            if (this.spriteModel.getHorizontalSpeed() == 0 && this.spriteModel.getVerticalSpeed() == 0) {
                this.setDestination(new Point(dishObject.getDestination()));
            }
            else {
                this.setDestination(this.fromPoint);
            }
          
        } else if (dishObject instanceof Bacterium && parent != dishObject && size >= dishObject.getSize()){
            try {
                this.eat(dishObject);
            } catch (NoSuchMethodException ex) {
                Logger.getLogger(Bolid.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(Bolid.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(Bolid.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                Logger.getLogger(Bolid.class.getName()).log(Level.SEVERE, null, ex);
            }
        } 
        else if (dishObject instanceof Bolid) {
            try {
                super.eat((Bacterium)dishObject);
            } catch (NoSuchMethodException ex) {
                Logger.getLogger(Bolid.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(Bolid.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(Bolid.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                Logger.getLogger(Bolid.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void update(long l) {
        if (this.parentCollision && !isInParent()) {
            this.parentCollision = false;
        }
        super.update(l);
    }

    private boolean isInParent() {
        int bolidX = this.getPosition().x + size / 2;
        int bolidY = this.getPosition().y + size / 2;

        int parentX = this.parent.getPosition().x + this.parent.getSize() / 2;
        int parentY = this.parent.getPosition().y + this.parent.getSize() / 2;

        double distance = (double) Math.sqrt(((bolidX - parentX) * (bolidX - parentX)) + ((bolidY - parentY) * (bolidY - parentY)));

        if (distance < this.parent.getSize() / 2 + size / 2) {
            return true;
        }
        return false;
    }

}
