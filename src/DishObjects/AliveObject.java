/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DishObjects;

import Specializations.Ration;
import Specializations.Specialization;
import agariounplugged.Dish;
import agariounplugged.DishObjectController;
import agariounplugged.Factories.AgarioFactory;
import events.ObjectGrowedEvent;
import events.ObjectGrowedListener;
import java.awt.Point;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dmitry
 */
public abstract class AliveObject extends DishObject {

    private final int JUNK_DISTANCE_COEFFICIENT = 2;
    
    protected DishObjectController controller = null;

    public void setController(DishObjectController controller) {
        this.controller = controller;
    }
    
    public DishObjectController getController() {
        return this.controller;
    }

    public AliveObject(AgarioFactory agarioFactory) {
        super(agarioFactory);
        eaten = new ArrayList<DishObject>();
    }

    protected Specialization specialization;

    /**
     * Массив съеденных объектов
     */
    private ArrayList<DishObject> eaten;

    /**
     * Событие роста объекта
     */
    private ObjectGrowedEvent oge = new ObjectGrowedEvent(this);

    /**
     * Слушатели события роста объекта
     */
    private ArrayList<ObjectGrowedListener> OGlisteners = new ArrayList<ObjectGrowedListener>();

    /**
     * Съесть объект
     *
     * @param obj съедаемый объект
     * @return признак успешного съедения
     */
    public boolean eat(DishObject obj) throws NoSuchMethodException, IllegalArgumentException, InvocationTargetException, InstantiationException {
        // Если съедаемый объект не входит в нашу специализацию - не есть его
        if (!this.specialization.canEat(obj, this)) {
            return false;
        }

        // Добавить в массив съеденных объектов
        eaten.add(obj);

        // Удалить съеденный объект с поля
        Dish.getInstance().removeObject(obj);

        // Проверить выполнение рациона
        Ration completedRation = specialization.completedRation(eaten);

        // Если выполнился какой-то рацион
        if (completedRation != null) {
            int eatenSize = 0;          // суммарные размеры съеденных объектов, входящих в выполненный рацион

            // Удалить из массива съеденных объектов объекты рациона
            Map<String, Integer> objects = completedRation.getRation();
            Set<String> types = objects.keySet();
            for (String type : types) {
                for (int i = 0; i < eaten.size(); ++i) {
                    // Если найден съеденный объект нужного типа
                    if (eaten.get(i).getType() == type) {
                        // Увеличить суммарный размер съеденных объектов текущего рациона на размер текущего элемента
                        eatenSize += eaten.get(i).getSize();

                        // Удалить его из съеденных объектов
                        eaten.remove(i);
                        i--;

                        // Уменьшить количество оставшихся востребованных объектов данного типа
                        objects.put(type, objects.get(type) - 1);

                        // Если набрано нужное количетсво объектов текущего типа - перейти кследующему
                        if (objects.get(type) == 0) {
                            break;
                        }
                    }
                }
            }

            // Получить количество испускаемых продуктов жизнедеятельности при выполнении текущего рациона
            int junkCount = completedRation.junkCount(eatenSize);

            // Увеличиться в размерах
            this.setSize(getSize() + completedRation.getSizeGrowth(eatenSize));

            // Испустить продукты жизнедеятельности
            for (int i = 0; i < junkCount; ++i) {
                throwJunk(completedRation.getJunkClass());
            }

            // Оповестить слушателей о росте
            fireObjectGrowed(getSize());
        }

        return true;
    }

    public String getType() {
        if (specialization == null) {
            return null;
        }
        return specialization.getType();
    }

    /**
     * Добавить слушателя роста объекта
     *
     * @param l слушатель объекта
     */
    public void addObjectGrowedListener(ObjectGrowedListener l) {
        OGlisteners.add(l);
    }

    /**
     * Очистить список слушателей роста объекта
     */
    public void clearObjectGrowedListener() {
        OGlisteners.clear();
    }

    /**
     * Оповестить слушателей об изменении размера объекта
     *
     * @param size новый размер объекта
     */
    protected void fireObjectGrowed(int size) {
        for (int i = 0; i < OGlisteners.size(); i++) {
            OGlisteners.get(i).growed(oge, this);
        }
    }

    private void throwJunk(Class junkClass) throws NoSuchMethodException, InstantiationException, IllegalArgumentException, InvocationTargetException {
        // Вычислить расстояние, на которое полетит объект
        int distance = (int) (getSize() * JUNK_DISTANCE_COEFFICIENT);

        // Вычислить случайное отклонение по оси Х от текущего объекта
        Random rand = new Random();
        int dx = rand.nextInt(2 * distance) - distance;

        // Вычислить отклонение по оси y, исходя из отклонения по оси x и дистанции по теореме Пифагора
        int dy = (int) Math.sqrt(distance * distance - dx * dx) * (rand.nextInt(2) == 0 ? -1 : 1);
        Point curPosition = getPosition();

        // Создать объект результата жизнедеятельности
        try {
            DishObject junk;
            junk = (DishObject) junkClass.getDeclaredConstructor(AgarioFactory.class).newInstance(this.agarioFactory);
            Dish.getInstance().addObject(junk, curPosition);

            // Задать точку назначения полёта
            junk.setDestination(new Point(curPosition.x + dx, curPosition.y + dy));
        } catch (InstantiationException ex) {
            Logger.getLogger(AliveObject.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(AliveObject.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setSpecialization(Specialization specialization) {
        // Проверить возможность улучшения до заданной специализации из текущей
        if (this.specialization != null && !this.specialization.availableUpgrades().contains(specialization)) {
            return;
        }

        // Улучшить специализацию
        clearObjectGrowedListener();
        this.specialization = specialization;
        addObjectGrowedListener(specialization.getObserver());

        this.setSize(getSize());
    }

}
