/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DishObjects;

import agariounplugged.Dish;
import agariounplugged.Factories.AgarioFactory;
import java.awt.Point;
import views.SpriteModel;

/**
 *
 * @author Dmitry
 */
public abstract class DishObject {

    private int BREAK_COEFFICIENT = 200;
    private int STOP_MOVE_RADIUS = 15;
    private int SPEED_PER_SIZE = 25;
    
    protected AgarioFactory agarioFactory;

    private double speed;

    protected Point destination;

    public DishObject(AgarioFactory agarioFactory) {
        this.agarioFactory = agarioFactory;
        this.spriteModel = agarioFactory.createSpriteModel();
    }

    protected int size;

    protected SpriteModel spriteModel;

    public void setDestination(Point destination) {
        this.destination = destination;
    }

    public void setSize(int size) {
        this.size = size;
        // Задать скорость относительно нового размера
        speed = SPEED_PER_SIZE / (double) this.size;
    }

    public int getSize() {
        return this.size;
    }

    public void update(long l) {
        this.updateSpeedVector();
        this.spriteModel.update(l);
    }

    public void setPosition(Point position) {
        int x = position.x;
        int y = position.y;
        this.spriteModel.setPosition(new Point(x, y));
        this.destination = new Point(position);
    }

    public Point getPosition() {
        int x = this.spriteModel.getPosition().x;
        int y = this.spriteModel.getPosition().y;
        return new Point(x, y);
    }
    
    public Point getDestination() {
        if (this.destination == null) {
            return new Point(0,0);
        }
        return this.destination;
    }

    public SpriteModel getSpriteModel() {
        return this.spriteModel;
    }

    private void updateSpeedVector() {
        // Если направление перемещения не задано, то не перемещаемся никуда
        if (destination != null) {

            // Определить расстояние от объекта до цели
            Point position = getPosition();
            int dx = destination.x - this.size/2 - position.x;
            int dy = destination.y - this.size/2 - position.y;
            int distance = (int) Math.sqrt(dx * dx + dy * dy);

            // Если цель достигнута
            if (dx * dx + dy * dy <= STOP_MOVE_RADIUS * STOP_MOVE_RADIUS) {
                this.spriteModel.setSpeed(0, 0);
            } // Иначе устанавливаем скорости по осям координат
            else {
                this.spriteModel.setSpeed(Math.min(speed, (double)distance / BREAK_COEFFICIENT) * dx / (Math.abs(dx) + Math.abs(dy)),
                        Math.min(speed, (double)distance / BREAK_COEFFICIENT) * dy / (Math.abs(dx) + Math.abs(dy)));
            }

            // Проверить выход за границу чашки
            if (this.getPosition().x <= 0 && this.spriteModel.getHorizontalSpeed() < 0) {
                this.spriteModel.setSpeed(0, this.spriteModel.getVerticalSpeed());
            }
            if (this.getPosition().x + this.getSize() >= Dish.getInstance().fieldWidth && this.spriteModel.getHorizontalSpeed() > 0) { //Магия нах
                this.spriteModel.setSpeed(0, this.spriteModel.getVerticalSpeed());
            }
            if (this.getPosition().y <= 0 && this.spriteModel.getVerticalSpeed() <= 0) {
                this.spriteModel.setSpeed(this.spriteModel.getHorizontalSpeed(), 0);
            }
            if (this.getPosition().y >= Dish.getInstance().fieldHeight - this.getSize() && this.spriteModel.getVerticalSpeed() > 0) {
                this.spriteModel.setSpeed(this.spriteModel.getHorizontalSpeed(), 0);
            }
        }
    }

    public abstract String getType();
    public abstract void collideWith(DishObject dishObject);
}
