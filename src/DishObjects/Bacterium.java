/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DishObjects;

import agariounplugged.Dish;
import agariounplugged.DishObjectController;
import agariounplugged.Factories.AgarioFactory;
import java.awt.Point;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dmitry
 */
public class Bacterium extends AliveObject {

    private final int DEFAULT_SIZE = 80;

    private boolean inInitCollision = true;

    public Bacterium(AgarioFactory agarioFactory) {
        super(agarioFactory);
        this.setSize(DEFAULT_SIZE);
    }

    /**
     * Выстрелить болид
     *
     * @param destination - точка на поле, в сторону которой полетит болид
     */
    public void throwBolid(Point destination) {
        if (this.size >= this.DEFAULT_SIZE / 1.5) {
            Bolid bolid = new Bolid(this.agarioFactory);
            bolid.setSpecialization(this.specialization);
            bolid.setSize(this.size / 3);
            Dish.getInstance().addObject(bolid, new Point(this.getPosition().x + size / 2 - bolid.getSize() / 2,
                    this.getPosition().y + size / 2 - bolid.getSize() / 2));
            bolid.setDestination(destination);

            bolid.setParent(this);
            this.setSize(this.size / 3 * 2);
        }
    }

    @Override
    public void update(long l) {
        super.update(l);
        if (this.controller != null) {
            this.controller.updateDishObjectPosition(this);
            this.controller.initThrowBolid(this);
        }
    }

    public void absorb(Bolid bolid) {
        this.setSize(this.size + bolid.getSize());
        Dish.getInstance().removeObject(bolid);
    }

    @Override
    public void collideWith(DishObject dishObject) {
        if (!(dishObject instanceof Bolid)) {
            try {
                this.eat(dishObject);
            } catch (NoSuchMethodException ex) {
                Logger.getLogger(Bacterium.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(Bacterium.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(Bacterium.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                Logger.getLogger(Bacterium.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (dishObject instanceof Bolid && ((Bolid) dishObject).getParent() == this) {
            if (!((Bolid) dishObject).isParentCollision()) {
                this.absorb((Bolid) dishObject);
            }
        }
    }

}
