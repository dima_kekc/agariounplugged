/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DishObjects;

import agariounplugged.Factories.AgarioFactory;

/**
 *
 * @author Dmitry
 */
public class Water extends PrimitiveObject{
    private final int DEFAULT_SIZE = 20;

    public Water(AgarioFactory agarioFactory) {
        super(agarioFactory);
        this.setSize(DEFAULT_SIZE);
    }

    @Override
    public String getType() {
        return "WATER";
    }
}
