/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DishObjects;

import agariounplugged.Factories.AgarioFactory;
import views.SpriteModel;

/**
 *
 * @author Dmitry
 */
public abstract class PrimitiveObject extends DishObject{

    public PrimitiveObject(AgarioFactory agarioFactory) {
        super(agarioFactory);
    }
    
    public void collideWith(DishObject dishObject){
    }
}
