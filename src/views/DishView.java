/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import agariounplugged.Factories.DishObjectViewFactory;
import com.golden.gamedev.object.Background;
import events.AddDishObjectListener;
import events.RemoveDishObjectListener;
import java.awt.Graphics2D;

/**
 *
 * @author Dmitry
 */
public abstract class DishView implements AddDishObjectListener, RemoveDishObjectListener {

    public abstract void render(Graphics2D gd);
}
