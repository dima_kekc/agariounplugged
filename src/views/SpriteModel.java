/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import java.awt.Point;

/**
 *
 * @author Dmitry
 */
public interface SpriteModel {
    void setPosition(Point p);
    Point getPosition();
    void setSpeed(double horizontalSpeed, double VerticalSpeed);
    double getHorizontalSpeed();
    double getVerticalSpeed();
    void update(long l);
}
