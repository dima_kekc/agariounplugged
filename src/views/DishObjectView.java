/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import DishObjects.DishObject;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

/**
 *
 * @author Dmitry
 */
public interface DishObjectView {
    
    void setDishObject(DishObject dishObject);
    
    DishObject getDishObject ();

    void setColor(Color color);

    void setIcon(BufferedImage icon);

    void render(Graphics2D gd);
}
