/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Specializations;

import DishObjects.Oxygen;

/**
 *
 * @author Dmitry
 */
public class MossBacteriumSpecialization extends Specialization{
    
    private MossBacteriumSpecialization(String type, int upgradeSize) {
        super(type, upgradeSize);
    }
    
    public MossBacteriumSpecialization() {
        super("BACTERIUM_MOSS", 9999);
        
        this.addRation(new Ration(Ration.HIGH_EFFICIENCY, Oxygen.class).addRule("CARBON_DIOXIDE")
                                                                                .addRule("LIGHT")
                                                                                .addRule("WATER"));
    }
    
}