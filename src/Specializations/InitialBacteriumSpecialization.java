/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Specializations;

import DishObjects.Agar;
import DishObjects.CarbonDioxide;

/**
 *
 * @author Dmitry
 */
public class InitialBacteriumSpecialization extends Specialization{
    
    private InitialBacteriumSpecialization(String type, int upgradeSize) {
        super(type, upgradeSize);
    }
    
    public InitialBacteriumSpecialization() {
        super("INITIAL_BACTERIUM", 100); //лучше 100
        this.addRation(new Ration(Ration.HIGH_EFFICIENCY, CarbonDioxide.class).addRule("AGAR")
                                                                                 .addRule("WATER")
                                                                                 .addRule("LIGHT"))
               .addRation(new Ration(Ration.HIGH_EFFICIENCY, CarbonDioxide.class).addRule("INITIAL_BACTERIUM"));
    }
    
}
