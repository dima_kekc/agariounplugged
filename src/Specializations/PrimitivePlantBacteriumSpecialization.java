/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Specializations;

import DishObjects.CarbonDioxide;
import DishObjects.Oxygen;

/**
 *
 * @author Dmitry
 */
public class PrimitivePlantBacteriumSpecialization extends Specialization{
    
    private PrimitivePlantBacteriumSpecialization(String type, int upgradeSize) {
        super(type, upgradeSize);
    }
    
    public PrimitivePlantBacteriumSpecialization() {
        super("PRIMITIVE_PLANT", 150);
        this.addRation(new Ration(Ration.AVARAGE_EFFICIENCY, Oxygen.class).addRule("CARBON_DIOXIDE")
                                                                                    .addRule("LIGHT")
                                                                                    .addRule("WATER"))
                      .addRation(new Ration(Ration.AVARAGE_EFFICIENCY, Oxygen.class).addRule("INITIAL_BACTERIUM"));
    }
    
}
