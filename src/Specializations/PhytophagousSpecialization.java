/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Specializations;

import DishObjects.CarbonDioxide;
import DishObjects.Oxygen;

/**
 *
 * @author Dmitry
 */
public class PhytophagousSpecialization extends Specialization{
    
    private PhytophagousSpecialization(String type, int upgradeSize) {
        super(type, upgradeSize);
    }
    
    public PhytophagousSpecialization() {
        super("PHYTOPHAGOUS_ANIMAL", 200);
        this.addRation(new Ration(Ration.AVARAGE_EFFICIENCY, CarbonDioxide.class).addRule("OXYGEN")
                                                                                               .addRule("WATER")
                                                                                               .addRule("PRIMITIVE_PLANT", 1, 1.25))
                          .addRation(new Ration(Ration.AVARAGE_EFFICIENCY, CarbonDioxide.class).addRule("OXYGEN")
                                                                                               .addRule("WATER")
                                                                                               .addRule("BACTERIUM_MOSS", 1, 1.25))
                          .addRation(new Ration(Ration.AVARAGE_EFFICIENCY, CarbonDioxide.class).addRule("OXYGEN")
                                                                                               .addRule("WATER")
                                                                                               .addRule("PARASITE_PLANT", 1, 1.25))
                          .addRation(new Ration(Ration.AVARAGE_EFFICIENCY, CarbonDioxide.class).addRule("OXYGEN")
                                                                                               .addRule("WATER")
                                                                                               .addRule("PREDATOR_PLANT", 1, 1.25));
    }
    
}
