/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Specializations;

import DishObjects.CarbonDioxide;
import DishObjects.Oxygen;

/**
 *
 * @author Dmitry
 */
public class TigerSpecialization extends Specialization{
    
    private TigerSpecialization(String type, int upgradeSize) {
        super(type, upgradeSize);
    }
    
    public TigerSpecialization() {
        super("BACTERIUM_TIGER", 999999); //лучше 100
        this.addRation(new Ration(Ration.LOW_EFFICIENCY, CarbonDioxide.class).addRule("OXYGEN")
                                                                                       .addRule("WATER")
                                                                                       .addRule("PRIMITIVE_ANIMAL", 1, 2))
                      .addRation(new Ration(Ration.LOW_EFFICIENCY, CarbonDioxide.class).addRule("OXYGEN")
                                                                                       .addRule("WATER")
                                                                                       .addRule("PHYTOPHAGOUS_ANIMAL", 1, 2))
                      .addRation(new Ration(Ration.LOW_EFFICIENCY, CarbonDioxide.class).addRule("OXYGEN")
                                                                                       .addRule("WATER")
                                                                                       .addRule("BACTERIUM_BUFFALO", 1, 2))
                     .addRation(new Ration(Ration.HIGH_EFFICIENCY, CarbonDioxide.class).addRule("OXYGEN")
                                                                                       .addRule("WATER")
                                                                                       .addRule("BACTERIUM_BUFFALO", 1, 1.5));
    }
    
}
