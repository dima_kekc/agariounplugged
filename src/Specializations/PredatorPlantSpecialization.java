/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Specializations;

import DishObjects.CarbonDioxide;
import DishObjects.Oxygen;

/**
 *
 * @author Dmitry
 */
public class PredatorPlantSpecialization extends Specialization{
    
    private PredatorPlantSpecialization(String type, int upgradeSize) {
        super(type, upgradeSize);
    }
    
    public PredatorPlantSpecialization() {
        super("PREDATOR_PLANT", 999999);
        this.addRation(new Ration(Ration.AVARAGE_EFFICIENCY, CarbonDioxide.class).addRule("OXYGEN")
                                                                                           .addRule("WATER")
                                                                                           .addRule("PRIMITIVE_ANIMAL", 1, 2))
                      .addRation(new Ration(Ration.AVARAGE_EFFICIENCY, CarbonDioxide.class).addRule("OXYGEN")
                                                                                           .addRule("WATER")
                                                                                           .addRule("PHYTOPHAGOUS_ANIMAL", 1, 2))
                      .addRation(new Ration(Ration.AVARAGE_EFFICIENCY, CarbonDioxide.class).addRule("OXYGEN")
                                                                                           .addRule("WATER")
                                                                                           .addRule("BACTERIUM_BUFFALO", 1, 2));
    }
    
}
