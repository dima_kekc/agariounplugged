package Specializations;

import DishObjects.AliveObject;
import DishObjects.DishObject;
import events.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Специализация бактерий и болидов
 */
public abstract class Specialization {

    /**
     * Рационы, относящиеся к текущей специализации
     */
    ArrayList<Ration> rations;

    /**
     * Тип специализации
     */
    private String type;

    /**
     * Список специализаций, доступных для улучшения текущей
     */
    private ArrayList<Specialization> availableUpgrades;

    /**
     * Размер объекта, при котором можно улучшать текущую специализацию
     */
    private int upgradeSize;

    /**
     * Запрос на изменение специализации
     */
    private AskForSpecializationEvent afse = new AskForSpecializationEvent(this);

    /**
     * Слушатели запроса на изменение специализации
     */
    private ArrayList<AskForSpecializationListener> AFSlisteners = new ArrayList<AskForSpecializationListener>();

    public Specialization(String type, int upgradeSize) {
        rations = new ArrayList<Ration>();
        availableUpgrades = new ArrayList<Specialization>();

        this.type = type;
        this.upgradeSize = upgradeSize;
    }

    /**
     * Получить тип специализцаии
     *
     * @return тип специализации
     */
    public String getType() {
        return type;
    }

    /**
     * Добавить рацион к текущей специализации
     *
     * @param ration добавляемый рацион
     * @return текущий экземпляр класса для повторного добавления элементов
     */
    public Specialization addRation(Ration ration) {
        rations.add(ration);
        return this;
    }

    /**
     * Проверить допустимость съедения одного объекта другим
     *
     * @param eaten съедаемый объект
     * @param eater съедающий объект
     * @return признак допустимости съедения
     */
    public boolean canEat(DishObject eaten, DishObject eater) {
        // Если объект можно съесть в каком-нибудь из доступных рационов, то его можно есть
        for (Ration ration : rations) {
            if (ration.canEat(eaten, eater)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Получить выполненный рацион
     *
     * @param eaten массив съеденных объектов
     * @return выполненный рацион или null, если ни один не выполнен
     */
    public Ration completedRation(ArrayList<DishObject> eaten) {
        // Распределить съеденные объекты по типам и количеству по каждому типу
        Map<String, Integer> sortedEaten = new HashMap<String, Integer>();
        for (DishObject obj : eaten) {
            String type = obj.getType();

            // Если объекты этого типа уже были съедены - прибавить к их количеству единицу, иначе создать новую пару ключ-значениесо значением 1
            int count = 1;
            if (sortedEaten.containsKey(type)) {
                count += sortedEaten.get(type);
            }
            sortedEaten.put(type, count);
        }

        // Проверить выполнение какого-нибудь рациона
        for (Ration ration : rations) {
            if (ration.rationCompleted(sortedEaten)) {
                return ration;
            }
        }

        return null;
    }

    /**
     * Добавить доступное улучшение специализации
     *
     * @param spec доступное улучшение специализации
     */
    public void addAvailableUpgrade(Specialization spec) {
        this.availableUpgrades.add(spec);
    }

    /**
     * Получить доступные улучшения специализации
     *
     * @return доступные улучшения специализации
     */
    public ArrayList<Specialization> availableUpgrades() {
        return this.availableUpgrades;
    }

    /**
     * Получить строковое название специализации
     *
     * @return строковое описание специализации
     */
    public String getName() {
        return this.getType().substring(0, 1).toUpperCase() + this.getType().substring(1).toLowerCase().replace('_', ' ');
    }

    /**
     * Слушатель события роста объекта
     */
    private class Observer implements ObjectGrowedListener {

        @Override
        public void growed(ObjectGrowedEvent e, AliveObject aliveObject) {
            if (aliveObject.getSize() >= upgradeSize) {
                fireAskForSpecialization(availableUpgrades, aliveObject);
            }
        }
    }

    /**
     * Получить слушателя события роста объекта
     *
     * @return слушатель события роста объекта
     */
    public ObjectGrowedListener getObserver() {
        return new Observer();
    }

    /**
     * Добавить слушателя роста объекта
     *
     * @param l слушатель объекта
     */
    public void addAskForSpecializationListener(AskForSpecializationListener l) {
        AFSlisteners.add(l);
    }

    /**
     * Оповестить слушателей об изменении размера объекта
     *
     * @param size новый размер объекта
     */
    protected void fireAskForSpecialization(ArrayList<Specialization> availableUpgrades, AliveObject obj) {
        for (int i = 0; i < AFSlisteners.size(); i++) {
            AFSlisteners.get(i).askForSpecialization(afse, availableUpgrades, obj);
        }
    }
}
