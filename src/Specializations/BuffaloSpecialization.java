/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Specializations;

import DishObjects.CarbonDioxide;
import DishObjects.Oxygen;

/**
 *
 * @author Dmitry
 */
public class BuffaloSpecialization extends Specialization{
    
    private BuffaloSpecialization(String type, int upgradeSize) {
        super(type, upgradeSize);
    }
    
    public BuffaloSpecialization() {
        super("BACTERIUM_BUFFALO", 999999);
        
        this.addRation(new Ration(Ration.LOW_EFFICIENCY, CarbonDioxide.class).addRule("OXYGEN")
                                                                                         .addRule("WATER")
                                                                                         .addRule("PRIMITIVE_PLANT", 1, 1.25))
                        .addRation(new Ration(Ration.LOW_EFFICIENCY, CarbonDioxide.class).addRule("OXYGEN")
                                                                                         .addRule("WATER")
                                                                                         .addRule("BACTERIUM_MOSS", 1, 1.25))
                        .addRation(new Ration(Ration.LOW_EFFICIENCY, CarbonDioxide.class).addRule("OXYGEN")
                                                                                         .addRule("WATER")
                                                                                         .addRule("PARASITE_PLANT", 1, 1.25))
                        .addRation(new Ration(Ration.LOW_EFFICIENCY, CarbonDioxide.class).addRule("OXYGEN")
                                                                                         .addRule("WATER")
                                                                                         .addRule("PREDATOR_PLANT", 1, 1.25))
                       .addRation(new Ration(Ration.HIGH_EFFICIENCY, CarbonDioxide.class).addRule("OXYGEN")
                                                                                         .addRule("WATER")
                                                                                         .addRule("BACTERIUM_MOSS", 1, 1.5));
    }
    
}