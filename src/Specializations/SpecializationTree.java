/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Specializations;

import events.AskForSpecializationListener;
import java.util.ArrayList;

/**
 *
 * @author Dmitry
 */
public class SpecializationTree {
    
    /**
     * Начальная специализация всех растущих объектов
     */
    private Specialization initial;
    private AskForSpecializationListener askForSpecializationListener;
    /**
     * Массив всех специализаций для задания им слушателя апгрейда
     */
    private ArrayList<Specialization> allSpecializationsInTree = new ArrayList<Specialization>();
    
    public void setAskForSpecializationListener(AskForSpecializationListener listener) {
        this.askForSpecializationListener = listener;
        // Задать всем специализациям слушателя апгрейдов
        for(Specialization spec : this.allSpecializationsInTree) {
            spec.addAskForSpecializationListener(this.askForSpecializationListener);
        }
    }
    
    public SpecializationTree () {
        this.initial = new InitialBacteriumSpecialization();
        allSpecializationsInTree.add(initial);
        
        Specialization primitivePlant = new PrimitivePlantBacteriumSpecialization();
        allSpecializationsInTree.add(primitivePlant);
        
        Specialization buffalo = new BuffaloSpecialization();
        allSpecializationsInTree.add(buffalo);
        Specialization moss = new MossBacteriumSpecialization();
        allSpecializationsInTree.add(moss);
        Specialization omnivore = new OmnivoreAnimalSpecialization();
        allSpecializationsInTree.add(omnivore);
        Specialization parasitePlant = new ParasitePlantSpecialization();
        allSpecializationsInTree.add(parasitePlant);
        Specialization phytophangus = new PhytophagousSpecialization();
        allSpecializationsInTree.add(phytophangus);
        Specialization predatorPlant = new PredatorPlantSpecialization();
        allSpecializationsInTree.add(predatorPlant);
        Specialization predator = new PredatorSpecialization();
        allSpecializationsInTree.add(predator);
        Specialization primitiveAnimal = new PrimitiveAnimalBacteriumSpecialization();
        allSpecializationsInTree.add(primitiveAnimal);
        Specialization tiger = new TigerSpecialization();
        allSpecializationsInTree.add(tiger);
        
        
        // Дерево
        initial.addAvailableUpgrade(primitivePlant);
        
        initial.addAvailableUpgrade(primitiveAnimal);

        primitivePlant.addAvailableUpgrade(moss);
        primitivePlant.addAvailableUpgrade(parasitePlant);
        primitivePlant.addAvailableUpgrade(predatorPlant);

        primitiveAnimal.addAvailableUpgrade(phytophangus);
        primitiveAnimal.addAvailableUpgrade(predator);
        primitiveAnimal.addAvailableUpgrade(omnivore);
        
        phytophangus.addAvailableUpgrade(buffalo);
        
        predator.addAvailableUpgrade(tiger);
    }
    
        /**
     * Получить базовую специализацию
     * @return базовая специализация дерева специализаций
     */
    public Specialization getInitialSpecialization() {
        return initial;
    }
    
}
