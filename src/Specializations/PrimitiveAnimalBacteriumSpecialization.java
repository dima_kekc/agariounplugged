/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Specializations;

import DishObjects.CarbonDioxide;
import DishObjects.Oxygen;

/**
 *
 * @author Dmitry
 */
public class PrimitiveAnimalBacteriumSpecialization extends Specialization{
    
    private PrimitiveAnimalBacteriumSpecialization(String type, int upgradeSize) {
        super(type, upgradeSize);
    }
    
    public PrimitiveAnimalBacteriumSpecialization() {
        super("PRIMITIVE_ANIMAL", 150);
        
        this.addRation(new Ration(Ration.AVARAGE_EFFICIENCY, CarbonDioxide.class).addRule("OXYGEN")
                                                                                            .addRule("WATER")
                                                                                            .addRule("INITIAL_BACTERIUM", 1, 1.2));
    }
    
}