/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Specializations;

import DishObjects.CarbonDioxide;
import DishObjects.Oxygen;

/**
 *
 * @author Dmitry
 */
public class ParasitePlantSpecialization extends Specialization{
    
    private ParasitePlantSpecialization(String type, int upgradeSize) {
        super(type, upgradeSize);
    }
    
    public ParasitePlantSpecialization() {
        super("PARASITE_PLANT", 999999);
        this.addRation(new Ration(Ration.LOW_EFFICIENCY, Oxygen.class).addRule("CARBON_DIOXIDE")
                                                                               .addRule("LIGHT")
                                                                               .addRule("WATER"))
                    .addRation(new Ration(Ration.HIGH_EFFICIENCY, Oxygen.class).addRule("PRIMITIVE_PLANT", 1, 1.5))
                    .addRation(new Ration(Ration.HIGH_EFFICIENCY, Oxygen.class).addRule("BACTERIUM_MOSS", 1, 1.5))
                    .addRation(new Ration(Ration.HIGH_EFFICIENCY, Oxygen.class).addRule("PARASITE_PLANT", 1, 1.5))
                    .addRation(new Ration(Ration.HIGH_EFFICIENCY, Oxygen.class).addRule("PREDATOR_PLANT", 1, 1.5));
    }
    
}
