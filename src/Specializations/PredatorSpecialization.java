/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Specializations;

import DishObjects.Oxygen;

/**
 *
 * @author Dmitry
 */
public class PredatorSpecialization extends Specialization{
    
    private PredatorSpecialization(String type, int upgradeSize) {
        super(type, upgradeSize);
    }
    
    public PredatorSpecialization() {
        super("PREDATOR_ANIMAL", 200);
        this.addRation(new Ration(Ration.AVARAGE_EFFICIENCY, Oxygen.class).addRule("CARBON_DIOXIDE")
                                                                                   .addRule("LIGHT")
                                                                                   .addRule("WATER"))
                        .addRation(new Ration(Ration.HIGH_EFFICIENCY, Oxygen.class).addRule("PRIMITIVE_ANIMAL", 1, 0.5))
                        .addRation(new Ration(Ration.HIGH_EFFICIENCY, Oxygen.class).addRule("PHYTOPHAGOUS_ANIMAL", 1, 0.5))
                        .addRation(new Ration(Ration.HIGH_EFFICIENCY, Oxygen.class).addRule("BACTERIUM_BUFFALO", 1, 0.5))
                        .addRation(new Ration(Ration.HIGH_EFFICIENCY, Oxygen.class).addRule("PREDATOR_ANIMAL", 1, 0.5))
                        .addRation(new Ration(Ration.HIGH_EFFICIENCY, Oxygen.class).addRule("PHYTOPHAGOUS_ANIMAL", 1, 0.5))
                        .addRation(new Ration(Ration.HIGH_EFFICIENCY, Oxygen.class).addRule("OMNIVORE_ANIMAL", 1, 0.5));
    }
    
}
