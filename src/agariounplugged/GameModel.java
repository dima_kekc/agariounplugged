/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agariounplugged;

import DishObjects.Agar;
import DishObjects.Bacterium;
import DishObjects.Bolid;
import DishObjects.DishObject;
import DishObjects.Light;
import DishObjects.Water;
import Specializations.SpecializationTree;
import agariounplugged.Factories.AgarioFactory;
import java.awt.Point;

/**
 *
 * @author Dmitry
 */
public class GameModel {
    
    private final int INITIAL_AGAR_COUNT = 100;
    private final int INITIAL_LIGHT_COUNT = 100;
    private final int INITIAL_WATER_COUNT = 100;
    private final int INITIAL_BACTERIUM_COUNT = 10;
    /*
    * Фабрика для оновных игровых элементов
     */
    private AgarioFactory agarioFactory;
    private DishObjectController playerController;
    private AIController aiController;
    private Bacterium player;
    private SpecializationTree specTree;

    public GameModel(AgarioFactory agarioFactory) {
        // Создать дерево специализаций
        this.specTree = new SpecializationTree();
        Dish.getInstance().setCollisionManager(agarioFactory.createCollisionManager());
        this.agarioFactory = agarioFactory;
    }

    public void update(long l) {
        Dish.getInstance().update(l);
    }

    void startGame() {
        // Создать игрока
        player = new Bacterium(this.agarioFactory);
        player.setSpecialization(this.specTree.getInitialSpecialization());
        // Создать контроллер
        playerController = agarioFactory.createPlayerController();
        player.setController(playerController);
        // Добавить объекты в чашку
        // Создать базовые частицы чашки Петри
        // Свет
        for (int i = 0; i < INITIAL_LIGHT_COUNT; ++i) {
            Dish.getInstance().addObjectToRandomPosition(new Light(this.agarioFactory));
        }
        // Агар
        for (int i = 0; i < INITIAL_AGAR_COUNT; ++i) {
            Dish.getInstance().addObjectToRandomPosition(new Agar(this.agarioFactory));
        }
        // Вода
        for (int i = 0; i < INITIAL_WATER_COUNT; ++i) {
            Dish.getInstance().addObjectToRandomPosition(new Water(this.agarioFactory));
        }
        // Создать компьютерные бактерии
        for (int i = 0; i < INITIAL_BACTERIUM_COUNT; ++i) {
            Bacterium bot = new Bacterium(this.agarioFactory);
            bot.setController(new AIController());
            bot.setSpecialization(this.specTree.getInitialSpecialization());
            Dish.getInstance().addObjectToRandomPosition(bot);
        }
        Dish.getInstance().addObject(player, new Point(1000, 100));
    }

    public Bacterium getPlayer() {
        return this.player;
    }

    public SpecializationTree getSpecializationTree() {
        return this.specTree;
    }
}
