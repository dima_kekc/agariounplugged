/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agariounplugged;

import agariounplugged.Factories.AgarioFactory;
import GTGE.factories.GTGEAgarioFactory;

/**
 *
 * @author Dmitry
 */
public class AgarIOUnplugged {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        AgarioFactory agarioFactory = new GTGEAgarioFactory();

        GameApplication gameApplication = agarioFactory.createGameApplication();
        gameApplication.start();
    }

}
