/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agariounplugged;

import DishObjects.DishObject;
import java.lang.reflect.InvocationTargetException;

/**
 *
 * @author Dmitry
 */
public abstract class CollisionManager {

    public void collided(DishObject first, DishObject second) throws NoSuchMethodException, InvocationTargetException, IllegalArgumentException, InstantiationException {
        first.collideWith(second);
        second.collideWith(first);
    }

    public abstract void addDishObject(DishObject dishObject);

    public abstract void removeDishObject(DishObject dishObject);

    public abstract void checkCollision();
}
