/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agariounplugged;

import DishObjects.AliveObject;
import DishObjects.Bacterium;
import Specializations.Specialization;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Dmitry
 */
public class AIController implements DishObjectController {

    /**
     * Вероятность смены направления на каждом кадре
     */
    final double CHANGE_DESTINATION_PROBABILITY = 0.02;
    /**
     * Вероятность выстрела болидом
     */
    final double THROW_BOLID_PROBABILITY = 0.0001;

    /**
     * Управление движением компьютерных бактерий
     */
    @Override
    public void updateDishObjectPosition(Bacterium bacterium) {
        Random rand = new Random();
        if (Math.random() < CHANGE_DESTINATION_PROBABILITY) {
            // Сгенерировать случайные новые координаты перемещения
            bacterium.setDestination(new Point(Math.abs(rand.nextInt(Dish.fieldWidth)), Math.abs(rand.nextInt(Dish.fieldHeight))));
        }
    }

    /**
     * Выбор специализации компьютерными бактериями
     *
     * @param availableUpgrades доступные специализации
     * @param obj бактерия, для которой осуществляется выбор
     */
    @Override
    public void chooseSpecialization(ArrayList<Specialization> availableUpgrades, AliveObject obj) {
        Random rand = new Random();
        obj.setSpecialization(availableUpgrades.get(rand.nextInt(availableUpgrades.size())));
    }

    @Override
    public void initThrowBolid(Bacterium bacterium) {
        Random rand = new Random();
        if (Math.random() < THROW_BOLID_PROBABILITY) {
            // Сгенерировать случайные координаты выстрела
            int x = rand.nextInt(Dish.getInstance().fieldWidth);

            // Вычислить отклонение по оси y, исходя из отклонения по оси x и дистанции по теореме Пифагора
            int y = rand.nextInt(Dish.getInstance().fieldHeight);

            bacterium.throwBolid(new Point(x, y));
        }
    }

}
