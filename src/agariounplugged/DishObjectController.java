/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agariounplugged;

import DishObjects.AliveObject;
import DishObjects.Bacterium;
import Specializations.Specialization;
import java.util.ArrayList;

/**
 *
 * @author Dmitry
 */
public interface DishObjectController {
    public void updateDishObjectPosition(Bacterium bacterium);
    public void initThrowBolid(Bacterium bacterium);
    public void chooseSpecialization(ArrayList<Specialization> availableUpgrades, AliveObject obj);
}
