/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agariounplugged;

import DishObjects.AliveObject;
import Specializations.Specialization;
import events.AskForSpecializationEvent;
import events.AskForSpecializationListener;
import events.ObjectGrowedEvent;
import events.ObjectGrowedListener;
import java.awt.Graphics2D;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import views.DishView;

/**
 *
 * @author Dmitry
 */
public class GameFrame {

    /**
     * GameFrame знает о GameModel, чтобы по результатам диалогов с
     * пользователем управлять началом игры
     */
    private GameModel gameModel;

    private DishView dishView;

    private Observer observer;

    GameFrame(DishView dishView, GameModel gameModel) {
        this.dishView = dishView;
        this.observer = new Observer();
        this.gameModel = gameModel;
        this.gameModel.getSpecializationTree().setAskForSpecializationListener(observer);
    }

    public void render(Graphics2D gd) {
        this.dishView.render(gd);
    }

    public DishView getDishView() {
        return this.dishView;
    }

    /**
     * Слушатель события роста объекта
     */
    private class Observer implements AskForSpecializationListener {

        @Override
        public void askForSpecialization(AskForSpecializationEvent e, ArrayList<Specialization> availableUpgrades, AliveObject obj) {
            
                obj.getController().chooseSpecialization(availableUpgrades, obj);
            
        }
    }
}
