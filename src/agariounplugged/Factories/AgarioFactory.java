/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agariounplugged.Factories;

import agariounplugged.AIController;
import agariounplugged.CollisionManager;
import agariounplugged.DishObjectController;
import agariounplugged.GameApplication;
import views.DishView;
import views.SpriteModel;

/**
 * Класс создает фабрики, порождающие объекты в чашке, collisionManager,
 * представления и объект игры
 *
 * @author Dmitry
 */
public abstract class AgarioFactory {

    public abstract GameApplication createGameApplication();

    public abstract DishView createDishView();

    public abstract SpriteModel createSpriteModel();

    public abstract DishObjectController createPlayerController();

    public DishObjectController createAIController() {
        return new AIController();
    }

    public abstract CollisionManager createCollisionManager();
}
