/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agariounplugged.Factories;

import DishObjects.DishObject;
import views.DishObjectView;

/**
 *
 * @author Dmitry
 */
public abstract class DishObjectViewFactory {
    public abstract DishObjectView createDishObjectView(DishObject dishObject);
}
