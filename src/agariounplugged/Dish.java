/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agariounplugged;

import DishObjects.DishObject;
import agariounplugged.Factories.AgarioFactory;
import events.AddDishObjectListener;
import events.RemoveDishObjectListener;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Dmitry
 */
public class Dish {
    
        /**
     * количество попыток, прежпринимаемых для успешного размещения объекта на пустой позиции на поле
     */
    final int OBJECT_ADD_ATTEMPTS_COUNT = 20;
    
    /**
     * Размеры поля
     */    
    public static int fieldWidth = 3000,
            fieldHeight = 1985;
    
    private ArrayList<DishObject> objects;
    private ArrayList<AddDishObjectListener> addListeners = new ArrayList<AddDishObjectListener>();
    private ArrayList<RemoveDishObjectListener> removeListeners = new ArrayList<RemoveDishObjectListener>();
    private CollisionManager collisionManager;
    
    public ArrayList<DishObject> getDishObjects() {
        return this.objects;
    }
    
    
    public static Dish getInstance() {
        if (instance == null) {
            instance = new Dish();
        }
        return instance;
    }
    
    public void setCollisionManager(CollisionManager collisionManager) {
        this.collisionManager = collisionManager;
    } 
    
    public CollisionManager getCollisionManager () {
        return this.collisionManager;
    }
    
    private static Dish instance;
    
    private Dish() {
        objects = new ArrayList<DishObject>();
    }
    
    public void removeObject(DishObject object) {
        objects.remove(object);
        this.fireObjectRemoved(object);
        this.collisionManager.removeDishObject(object);
    }
    
    public void addObject(DishObject object, Point point) {
        object.setPosition(point);
        objects.add(object);
        this.collisionManager.addDishObject(object);
        this.fireObjectAdded(object);
    }
    
    public void update(long l) {
        for(int i = 0; i< objects.size(); ++i) {
            objects.get(i).update(l);
        }
        this.collisionManager.checkCollision();
    }
    
    /**
     * Добавить объект на поле в случайную свободную позицию
     * @param obj добавляемый объект
     * @return признак успешного добавления объекта
     */
    public boolean addObjectToRandomPosition(DishObject obj) {
        // Предпринять некоторое количество попыток разместить объект
        Random rand = new Random();
        for (int i = 0; i < OBJECT_ADD_ATTEMPTS_COUNT; ++i) {
            // Сгенерировать случайные координаты для объекта
            Point position = new Point(rand.nextInt(fieldWidth), rand.nextInt(fieldHeight));
            // Попытаться добавить объект по данным координатам
            if (isFreePosition(position, obj)) {
                addObject(obj, position);
                return true;
            }
        }
        return false;
    }
    
    /**
     * Проверить, можно ли в заданной позиции разместить заданный объект без пересечений с остальными
     * @param position позиция, в которую нужно разместить объект
     * @param obj добавляемый объект
     * @return доступность позиции для размещения объекта
     */
    private boolean isFreePosition(Point position, DishObject obj) {
        int x1 = (int) position.getX(), y1 = (int) position.getY();
        
        // Проверить выход за границы поля
        if (x1 - obj.getSize() / 2.0 < 0 || x1 + obj.getSize() / 2.0 >= fieldWidth ||
                y1 - obj.getSize() / 2.0 < 0 || y1 + obj.getSize() / 2.0 >= fieldHeight)
            return false;
        
        for (DishObject curObj : objects) {
            // Координаты центра текущего объекта
            int x2 = (int) curObj.getPosition().getX(), y2 = (int) curObj.getPosition().getY();
            
            // Подсчёт расстояния между центрами центрами объектов по теореме Пифагора
            double distance = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
            
            // Проверка наложения
            if (distance <= (obj.getSize() + curObj.getSize()) / 2.0)
                return false;
        }
        return true;
    }
    
    public boolean isFreePosition(Point point) {
        //to do
        return false;
    }
    
    public void addObjectAddedListener(AddDishObjectListener listener) {
        this.addListeners.add(listener);
    }
    
    public void addObjectRemovedListener (RemoveDishObjectListener listener) {
        this.removeListeners.add(listener);
    }
    
    private void fireObjectAdded(DishObject dishObject) {
        for (AddDishObjectListener listener : this.addListeners) {
            listener.dishObjectAdded(dishObject);
        }
    }
    
    private void fireObjectRemoved(DishObject dishObject) {
        for (RemoveDishObjectListener listener : this.removeListeners) {
            listener.dishObjectRemoved(dishObject);
        }
    }
}
