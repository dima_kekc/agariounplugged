/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agariounplugged;

import agariounplugged.Factories.AgarioFactory;
import java.awt.Graphics2D;

/**
 *
 * @author Dmitry
 */
public abstract class GameApplication {

    public static int screenWidth = 1024,
            screenHeight = 768;
    protected GameFrame gameFrame;
    protected GameModel gameModel;

    public GameApplication(AgarioFactory agarioFactory) {
        this.gameModel = new GameModel(agarioFactory);
        this.gameFrame = new GameFrame(agarioFactory.createDishView(), this.gameModel);
    }

    public void update(long l) {
        this.gameModel.update(l);
    }

    public void render(Graphics2D gd) {
        this.gameFrame.render(gd);
    }
    
    public void start () {
        gameModel.startGame();
    }
}
